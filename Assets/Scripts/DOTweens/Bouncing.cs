﻿using DG.Tweening;
using UnityEngine;

namespace AmayaSoft
{
    public class Bouncing : MonoBehaviour
    { 
        [SerializeField] 
        private bool _isBouncing;
        public bool IsBouncing { get { return _isBouncing; } set { _isBouncing = value; } }


        private void Awake()
        {
            DOTween.Init();
        }


        private void Bounce(float endValue, Vector3 startScale)
        {
            if (_isBouncing)
            {
                transform.localScale = startScale;
                transform.DOScale(endValue, 1f).SetEase(Ease.OutBounce);
            }
        }


        public void TileBounce()
        {
            Bounce(1.5f, new Vector3(0.1f, 0.1f, 1f));
        }


        public void ContentCorrectBounce()
        {
            Bounce(1f, new Vector3(0.1f, 0.1f, 1f));
        }


        public void ContentIncorrectBounce()
        {
            if (_isBouncing)
            {
                transform.localPosition = new Vector3(0.1f, 0, 0);
                transform.DOLocalMoveX(0f, 0.3f).SetEase(Ease.OutBounce);
            }
        }
    }
}