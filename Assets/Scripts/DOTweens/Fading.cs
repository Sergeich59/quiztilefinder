﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


namespace AmayaSoft
{
    public class Fading : MonoBehaviour
    {
        [SerializeField]
        private bool _isText;

        [SerializeField]
        private MeshRenderer _meshRenderer;

        [SerializeField]
        private Text _text;

        private Material _material;


        private void Awake()
        {
            if (_isText)
            {
                _material = _text.material;
            }
            else
            {
                _material = _meshRenderer.material;
            }

            DOTween.Init();
        }


        public void FadeIn()
        {
            _material.color = new Color(_material.color.r, _material.color.g, _material.color.b, 0);
            _material.DOFade(1, 1.5f);
        }


        public void FadeOut()
        {
            _material.color = new Color(_material.color.r, _material.color.g, _material.color.b, 1);
            _material.DOFade(0, 1);
        }
    }
}