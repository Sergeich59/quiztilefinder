﻿using UnityEngine;
using UnityEngine.Events;


namespace AmayaSoft
{
    public class Tile : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        [SerializeField]
        private Transform _content;

        [SerializeField]
        private Bouncing _bouncingTile;

        [SerializeField]
        private Bouncing _bouncingContent;

        [SerializeField]
        private ClickButton _buttonClicker;

        private GameStatus _gameStatus;

        private string _identifier;

        private bool _isRightTile = false;

        public Bouncing BouncingTile => _bouncingTile;
        public Bouncing BouncingContent => _bouncingContent;
        public ClickButton ButtonClicker => _buttonClicker;

        public UnityEvent CorrectEvent;

        public UnityEvent IncorrectEvent;


        public Tile Init(string identifier, Sprite sprite, float rotationOffset, bool isRightTile, GameStatus gameStatus)
        {
            _identifier = identifier;
            _spriteRenderer.sprite = sprite;
            _content.Rotate(0, 0, rotationOffset);
            _isRightTile = isRightTile;
            _gameStatus = gameStatus;
            return this;
        }


        public void ClearTile()
        {
            _spriteRenderer.sprite = null;
            _identifier = string.Empty;
            _content.rotation = Quaternion.Euler(0,0,0);
            _isRightTile = false;
        }

        
        public void SelectTile()
        {
            if (_isRightTile)
            {
                CorrectEvent?.Invoke();
                _gameStatus.NextLevel();
            }
            else
            {
                IncorrectEvent?.Invoke();
            }
        }
    }
}