﻿using UnityEngine;


namespace AmayaSoft
{
    [CreateAssetMenu(fileName = "New TilesBundle", menuName = "Scriptable Objects/Tiles Bundle", order = 1)]
    public class TilesBundle : ScriptableObject
    {
        [SerializeField] 
        private TileData[] _tilesData;

        public TileData[] TilesData => _tilesData;
    }
}