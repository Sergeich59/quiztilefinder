﻿using UnityEngine;


namespace AmayaSoft
{
    [CreateAssetMenu(fileName = "New TileData", menuName = "Scriptable Objects/Tile Data", order = 1)]
    public class TileData : ScriptableObject
    {
        [SerializeField] 
        private string _identifier;

        [SerializeField] 
        private Sprite _sprite;

        [SerializeField] 
        private float _rotationOffset;

        public string Identifier => _identifier;
        public Sprite Sprite => _sprite;
        public float RotationOffset => _rotationOffset;
    }
}