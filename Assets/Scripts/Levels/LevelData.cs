﻿using UnityEngine;


namespace AmayaSoft
{
    [CreateAssetMenu(fileName = "New LevelData", menuName = "Scriptable Objects/Level Data", order = 1)]
    public class LevelData : ScriptableObject
    {
        [SerializeField] 
        private int _countTiles;

        [SerializeField] 
        private TilesBundle[] _tileBundles;

        public int CountTiles => _countTiles;
        public TilesBundle[] TileBundles => _tileBundles;
    }
}