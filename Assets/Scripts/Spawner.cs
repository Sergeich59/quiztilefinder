﻿using UnityEngine;


namespace AmayaSoft
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] 
        private GameObject _tilePrefab;

        private Grid _grid;


        private void Awake()
        {
            _grid = GetComponent<Grid>();
        }


        public void SpawnTiles(Transform[] coordinates, int countTiles)
        {
            for (int i = _grid.CurrentTiles.Count; i < countTiles; i++)
            {
                _grid.CurrentTiles.Add(Instantiate(_tilePrefab, coordinates[i].position, Quaternion.identity).GetComponent<Tile>());
            }
        }
    }
}