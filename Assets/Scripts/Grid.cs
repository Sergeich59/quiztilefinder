﻿using UnityEngine;
using System.Collections.Generic;


namespace AmayaSoft
{
    public class Grid : MonoBehaviour
    {
        [SerializeField] 
        private Transform[] _coordinates;

        private Spawner _spawner;

        private HashSet<Tile> _currentTiles = new HashSet<Tile>();
        public HashSet<Tile> CurrentTiles => _currentTiles;
        public Transform[] Coordinates => _coordinates;


        private void Awake()
        {
            _spawner = GetComponent<Spawner>();
        }


        private void ClearAllTiles()
        {
            foreach (Tile tile in _currentTiles)
            {
                tile.ClearTile();
            }
        }


        private void InitializeAllTiles(TileData[] tilesData, TileData currentAnswerData, GameStatus gameStatus)
        {
            int i = 0;
            foreach (Tile tile in _currentTiles)
            {
                tile.Init(tilesData[i].Identifier, tilesData[i].Sprite, tilesData[i].RotationOffset, currentAnswerData.Identifier == tilesData[i].Identifier, gameStatus);
                i++;
            }
        }


        public void StartTilesSpawn(TileData[] tilesData, TileData currentAnswerData, GameStatus gameStatus)
        {
            ClearAllTiles();
            _spawner.SpawnTiles(_coordinates, tilesData.Length);
            InitializeAllTiles(tilesData, currentAnswerData, gameStatus);
            SetTileContentBouncing(true);
            SetTileInteractable(true);
        }

        public void RemoveAllTiles()
        {
            foreach(Tile tile in _currentTiles)
            {
                Destroy(tile.gameObject);
            }
            _currentTiles.Clear();
        }

        
        public void SetTileContentBouncing(bool isBounce)
        {
            foreach (Tile tile in _currentTiles)
            {
                tile.BouncingContent.IsBouncing = isBounce;
            }
        }


        public void SetTileInteractable(bool isInteract)
        {
            foreach (Tile tile in _currentTiles)
            {
                tile.ButtonClicker.IsInteractable = isInteract;
            }
        }
    }
}