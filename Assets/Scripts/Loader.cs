﻿using UnityEngine;
using UnityEngine.Events;


namespace AmayaSoft
{
    public class Loader : MonoBehaviour
    {
        private float loadDelay = 2f;

        public UnityEvent LoadEvent;


        private void InvokeLoadGame()
        {
            LoadEvent?.Invoke();
        }


        public void LoadGame()
        {
            Invoke(nameof(InvokeLoadGame), loadDelay);
        }
    }
}