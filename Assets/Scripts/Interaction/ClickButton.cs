﻿using UnityEngine;
using UnityEngine.Events;


namespace AmayaSoft
{
    public class ClickButton : MonoBehaviour
    {
        [SerializeField] 
        private UnityEvent ClickEvent;

        private bool _isInteractable;
        public bool IsInteractable { set { _isInteractable = value; } }


        private void OnMouseDown()
        {
            if (_isInteractable) ClickEvent?.Invoke();
        }
    }
}