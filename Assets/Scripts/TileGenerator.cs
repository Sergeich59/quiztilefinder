﻿using UnityEngine;
using System;


namespace AmayaSoft
{
    public class TileGenerator : MonoBehaviour
    {
        private TilesBundle _tilesBundle;

        private TileData _currentAnswerTileData;

        private TileData[] _usedAnswersTileData;

        public TilesBundle TilesBundle { get { return _tilesBundle; } set { _tilesBundle = value; } }
        public TileData CurrentAnswerTile { get { return _currentAnswerTileData; } set { _currentAnswerTileData = value; } }
        public TileData[] UsedAnswersTileData { get { return _usedAnswersTileData; } set { _usedAnswersTileData = value; } }


        public TileData[] GenerateTilesData(int countTiles, int currentLevel)
        {
            TileData[] tilesData = new TileData[countTiles];
            TileData tempTileData;

            for (int i = 0; i < countTiles;)
            {
                tempTileData = _tilesBundle.TilesData[UnityEngine.Random.Range(0, _tilesBundle.TilesData.Length)];

                if (Array.Exists(tilesData, element => element == tempTileData) == false)
                {
                    tilesData[i] = tempTileData;
                    i++;
                }
            }

            _currentAnswerTileData = GetAnswerTile(tilesData, currentLevel);

            return tilesData;
        }


        public TileData GetAnswerTile(TileData[] tilesData, int currentLevel)
        {
            TileData tempTileData;

            while (true)
            {
                tempTileData = tilesData[UnityEngine.Random.Range(0, tilesData.Length)];
                if (Array.Exists(_usedAnswersTileData, element => element == tempTileData) == false)
                {
                    _usedAnswersTileData[currentLevel] = tempTileData;
                    return tempTileData;
                }
            }
        }
    }
}