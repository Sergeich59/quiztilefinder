﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace AmayaSoft
{
    public class GameStatus : MonoBehaviour
    {
        [SerializeField] 
        private LevelData[] _levelsData;

        [SerializeField] 
        private Grid _grid;

        [SerializeField]
        private Text _goalText;

        [SerializeField]
        private TileGenerator _tileGenerator;

        private int _currentLevel;

        private float _nextLevelDelay = 1.5f;

        public UnityEvent StartGameEvent;

        public UnityEvent WinEvent;


        private void Awake()
        {
            _tileGenerator.UsedAnswersTileData = new TileData[_levelsData.Length];
        }


        private void Start()
        {
            StartGameEvent?.Invoke();
        }


        public void StartLevel()
        {
            LevelData levelData = _levelsData[_currentLevel];
            _tileGenerator.TilesBundle = levelData.TileBundles[Random.Range(0, levelData.TileBundles.Length)];
            _grid.StartTilesSpawn(_tileGenerator.GenerateTilesData(levelData.CountTiles, _currentLevel), _tileGenerator.CurrentAnswerTile, this);
            _goalText.text = "Find:  " + _tileGenerator.CurrentAnswerTile.Identifier;

            if (_currentLevel == 0)
            {
                foreach (Tile tile in _grid.CurrentTiles)
                {
                    tile.BouncingTile.TileBounce();
                }
            }
        }


        public void NextLevel()
        {
            _currentLevel++;
            _grid.SetTileContentBouncing(false);
            _grid.SetTileInteractable(false);

            if (_currentLevel >= _levelsData.Length)
            {
                WinEvent?.Invoke();
            }
            else
            {
                Invoke(nameof(StartLevel), _nextLevelDelay);
            }
        }


        public void RestartGame()
        {
            _currentLevel = 0;
            _grid.RemoveAllTiles();
            _tileGenerator.UsedAnswersTileData = new TileData[_levelsData.Length];
            StartLevel();
        }
    }
}